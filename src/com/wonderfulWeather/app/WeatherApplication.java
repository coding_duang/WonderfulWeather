package com.wonderfulWeather.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import com.wonderfulWeather.app.model.County;
import com.wonderfulWeather.app.model.WeatherDB;
import com.wonderfulWeather.app.util.CommonUtility;

/**
 * Created by kevin on 2014/12/26.
 * WeatherApplication
 */
public class WeatherApplication extends Application {
    private static Context context;
    private static County currentCounty;

    public void onCreate()
    {
        context=getApplicationContext();
    }

    public static Context getContext()
    {
        return  context;
    }

    public static void setCurrentCounty(County c)
    {
        CommonUtility.SaveWeatherCode(context.getSharedPreferences(CommonUtility.SHARED_PREF_NAME,MODE_PRIVATE),"",c.getCode(),"");
        currentCounty=c;
    }

    public static County getCurrentCounty()
    {
        if(currentCounty!=null) {
            return currentCounty;
        }
        else
        {
            SharedPreferences preferences=context.getSharedPreferences(CommonUtility.SHARED_PREF_NAME, MODE_PRIVATE);
            //默认北京
            return WeatherDB.getInstance(context).QueryCounty(preferences.getString("location","101010100")).get(0);
        }
    }

}
